#!/bin/bash

docker rmi $(docker images | grep "\d*\.\d*\.\d*\-\d*\-[a-z0-9]*" | awk '{print $1 ":" $2}')
